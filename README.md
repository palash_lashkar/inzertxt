# insertFileNameInImage

For a compliance audit, I needed to take few hundred screenshots and name them meaningfully and "put the file name in the image". 
This python script helped me to do only "put the file name in the image".  
This project has only 2 files, 1 property file and 1 driver file. 

Driver files : 
insertFileNameInImage.py

Property File;: 
projProperty.py


## Contents of projProperty.py ##

##where all target files sit 
g_imageSourceStr = 'C:\Users\mlashkar\Documents\UAT\UAT_images\UAT-11'

##where you would like tyo save all modified files 
g_imageSaveDestinationStr = 'C:\Users\mlashkar\Documents\UAT\UAT_images\UAT-11\tmp'


##How big the text needs to be comapred to the input file 
g_textSizeFontFactor = 0.18

##The Font-face of the text
g_textFontType= "arial.ttf"

##X-position of the inserted text calculated by image width * thisFactor 
g_textLocXCoordinanceFactor = 0.8

##y-position of the inserted text 
g_textLocYCoordinance = 5

##Can take a backup if set to true
g_TakeBackupBln = False

##The various input file types 
g_inputFileType_1 = 'png' 
g_inputFileType_2 = 'gif' 

##The Ouput file TYPE 
g_outputFileType = 'PNG' 


