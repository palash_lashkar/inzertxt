#! python3


import shutil
import os
import os.path
from properties.p import Property
import projProperty
from datetime import datetime
from PIL import Image, ImageDraw, ImageFont



def watermark_image_with_text_simple(p_filename, p_text): 
    mainImage = Image.open(p_filename); 

    watermark = Image.new("RGBA", mainImage.size)

    waterdraw = ImageDraw.ImageDraw(watermark, "RGBA")
    
    fontsize = 1  # starting font size
    
    # portion of image width you want text width to be
        
    font = ImageFont.truetype(projProperty.g_textFontType, fontsize)
    while font.getsize(p_text)[0] < projProperty.g_textSizeFontFactor*mainImage.size[0]:
        # iterate until the text size is just larger than the criteria
        fontsize += 1
        font = ImageFont.truetype(projProperty.g_textFontType, fontsize)
    
    # optionally de-increment to be sure it is less than criteria
    fontsize -= 1
    font = ImageFont.truetype(projProperty.g_textFontType, fontsize)

  
    waterdraw.text((mainImage.size[0]*projProperty.g_textLocXCoordinanceFactor, projProperty.g_textLocYCoordinance), p_text, font=font, fill=(0,0,0,0))
    
    watermask = watermark.convert("L").point(lambda x: min(x, 150))
    
    watermark.putalpha(watermask)

    mainImage.paste(watermark, None, watermark)
    
    mainImage.save( p_filename, projProperty.g_outputFileType)
    

def main():

    #Take a backup is setting says 
    if (projProperty.g_TakeBackupBln): 
        os.chdir(projProperty.g_imageSourceStr)
        shutil.copytree(os.getcwd(), 'directory_backup_{1}'.format(projProperty.g_imageSourceStr, datetime.now().isoformat()).replace(':', '_'))
    else: 
        print ( "Setting dictated to take No backup !\n")

    
    for filename in os.listdir(projProperty.g_imageSourceStr):
        if filename.lower().endswith(projProperty.g_inputFileType_1) or filename.lower().endswith(projProperty.g_inputFileType_2):
            print ( 'Processing File :' + filename )
            if filename is not None:

                try: 
                    watermark_image_with_text_simple (filename, filename) 
                except Exception:
                    print ("Issues encountered with "+ filename + "  .... moving on to next item\n\n" )
                    pass                


if __name__ == '__main__':
    main()